<?php

/**
 * @file
 * Provide views data that isn't tied to any other module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_views_data_alter().
 *
 * See EntityViewsData::getViewsData() for more details.
 */
function views_sort_options_weight_views_data_alter(array &$data) {
  $entity_type_manager = \Drupal::entityTypeManager();
  /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
  $entity_field_manager = \Drupal::service('entity_field.manager');

  $entity_types = $entity_type_manager->getDefinitions();
  // @todo - add alter here for allowed entity types?

  foreach ($entity_types as $entity_type) {
    // Skips entity if it is not allowed to use Views.
    if (!$entity_type->getHandlerClass('views_data')) {
      continue;
    }

    /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $entity_type_storage */
    $entity_type_storage = $entity_type_manager->getStorage($entity_type->id());

    $base_table = $entity_type->getBaseTable() ?: $entity_type->id();
    $views_revision_base_table = NULL;
    $revisionable = $entity_type->isRevisionable();

    $revision_table = '';
    if ($revisionable) {
      $revision_table = $entity_type->getRevisionTable() ?: $entity_type->id() . '_revision';
    }

    $translatable = $entity_type->isTranslatable();
    $data_table = '';
    if ($translatable) {
      $data_table = $entity_type->getDataTable() ?: $entity_type->id() . '_field_data';
    }

    // Load all typed data definitions of all fields. This should cover each of
    // the entity base, revision, data tables.
    $field_definitions = $entity_field_manager->getBaseFieldDefinitions($entity_type->id());
    /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
    if ($table_mapping = $entity_type_storage->getTableMapping($field_definitions)) {
      // Fetch all fields that can appear in both the base table and the data
      // table.
      $entity_keys = $entity_type->getKeys();
      $duplicate_fields = array_intersect_key(
        $entity_keys,
        array_flip(['id', 'revision', 'bundle'])
      );

      foreach ($table_mapping->getTableNames() as $table) {
        foreach ($table_mapping->getFieldNames($table) as $field_name) {
          // To avoid confusing duplication in the user interface, for fields
          // that are on both base and data tables, only add them on the data
          // table (same for revision vs. revision data).
          if ($data_table && ($table === $base_table || $table === $revision_table) && in_array($field_name, $duplicate_fields)) {
            continue;
          }

          // Adds plugin for bundles sorting.
          if ($field_name == $entity_type->getKey('bundle')) {
            if (isset($data[$table][$field_name]['sort'])) {
              $additional_field = "{$field_name}__sort_bundles_weight";
              $data[$table][$additional_field] = [
                'title' => $data[$table][$field_name]['title'] . ' ' . t('(set weight)'),
                'sort' => $data[$table][$field_name]['sort'],
              ];
              $data[$table][$additional_field]['sort']['id'] = 'extended_sort_by_bundles_weight';
              $data[$table][$additional_field]['sort']['field'] = $field_name;
            }
            continue;
          }

          // Adds plugin for field with allowed type.
          if (in_array($field_definitions[$field_name]->getType(), _views_sort_options_get_allowed_field_types())) {
            foreach ($table_mapping->getColumnNames($field_name) as $field_column_name => $schema_field_name) {
              if (isset($data[$table][$schema_field_name]['sort'])) {
                $additional_field = "{$field_name}__sort_options_weight";
                $data[$table][$additional_field] = [
                  'title' => $data[$table][$schema_field_name]['title'] . ' ' . t('(set weight)'),
                  'sort' => $data[$table][$schema_field_name]['sort'],
                ];
                $data[$table][$additional_field]['sort']['id'] = 'extended_sort_by_options_weight';
                $data[$table][$additional_field]['sort']['field'] = $field_name;
                $data[$table][$additional_field]['sort']['field_name'] = $field_name;
                break;
              }
            }
          }
        }
      }
    }
  }

  // Adds support for the User Role field.
  if (isset($data['user__roles']['roles_target_id'])) {
    $data['user__roles']['roles_target_id____sort_options_weight'] = [
      'title' => $data['user__roles']['roles_target_id']['title'] . ' ' . t('(set weight)'),
      'sort' => [
        'id' => 'extended_sort_by_user_role_weight',
        'field' => 'roles_target_id',
      ],
    ];
  }
}

/**
 * Implements hook_field_views_data_alter().
 */
function views_sort_options_weight_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {
  if (!in_array($field_storage->getType(), _views_sort_options_get_allowed_field_types())) {
    return $data;
  }

  foreach ($data as $table_name => $table_data) {
    foreach ($table_data as $field_name => $field_data) {
      // Most likely, this is "field_***_value" field.
      if (isset($field_data['sort'])) {
        $additional_field = "{$field_name}__sort_options_weight";
        $data[$table_name][$additional_field] = [
          'title' => $data[$table_name][$field_name]['title'] . ' ' . t('(set weight)'),
          'group' => $data[$table_name][$field_name]['group'],
          'sort' => $data[$table_name][$field_name]['sort'],
        ];
        $data[$table_name][$additional_field]['sort']['id'] = 'extended_sort_by_options_weight';
        break;
      }
    }
  }
}

/**
 * Get allowed field types.
 *
 * @return array
 *   The list of allowed field types.
 */
function _views_sort_options_get_allowed_field_types() {
  // @todo - add alter here for adding/removing allowed field types?
  return ['list_string', 'list_float', 'list_integer'];
}
