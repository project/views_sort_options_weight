<?php

namespace Drupal\views_sort_options_weight\Plugin\views\sort;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\sort\Standard;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExtendedSortByOptionWeightBase.
 *
 * @package Drupal\oktopro_financial_tool\Plugin\views\sort
 */
abstract class ExtendedSortByWeightBase extends Standard {

  const EMPTY_VALUE_IDENTIFIER = '__empty_value__';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityFieldManagerInterface $field_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $weight = 0;
    foreach ($this->getSortOptionsList() as $key => $label) {
      $options[$key] = ['default' => ++$weight];
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    foreach ($this->getSortOptionsList() as $key => $label) {
      $form[$key] = [
        '#title' => $label,
        '#type' => 'number',
        '#default_value' => $this->options[$key],
        '#description' => $this->t('This number represents %label type.', ['%label' => $label]),
        '#required' => TRUE,
      ];
    }
  }

  /**
   * Called to add the sort to a query.
   */
  public function query() {
    $sort_options_list = $this->getSortOptionsList();
    if (!$sort_options_list) {
      return;
    }

    $this->ensureMyTable();
    $field_alias = $this->query->addField($this->tableAlias, $this->realField);
    $query_field_name = $this->tableAlias . '.' . $this->realField;

    // Builds sorting expression.
    $expression = "CASE ";
    foreach ($sort_options_list as $key => $label) {
      switch ($key) {
        case static::EMPTY_VALUE_IDENTIFIER:
          $expression .= "WHEN {$query_field_name} IS NULL THEN {$this->options[$key]} ";
          break;

        default:
          $expression .= "WHEN {$query_field_name} = '{$key}' THEN {$this->options[$key]} ";
          break;
      }
    }
    $expression .= "ELSE 1000 END";

    // Add the field.
    $this->query->addOrderBy(NULL, $expression, $this->options['order'], $field_alias);
  }

  /**
   * Get the list of options which have weight.
   *
   * @return array
   *   The list of sortable options.
   */
  abstract protected function getSortOptionsList();

}
