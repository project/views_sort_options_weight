<?php

namespace Drupal\views_sort_options_weight\Plugin\views\sort;

/**
 * Default implementation of the base sort plugin.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("extended_sort_by_options_weight")
 */
class ExtendedSortByOptionsWeight extends ExtendedSortByWeightBase {

  /**
   * {@inheritdoc}
   */
  protected function getSortOptionsList() {
    $entity_fields_definitions = $this->entityFieldManager->getFieldStorageDefinitions($this->definition['entity_type']);

    if (!isset($entity_fields_definitions[$this->definition['field_name']])) {
      throw new \Exception('Field definition not found! Field: @field_name', ['@field_name' => $this->definition['field_name']]);
    }

    /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition */
    $field_definition = $entity_fields_definitions[$this->definition['field_name']];
    $allowed_options = options_allowed_values($field_definition);

    $list = [];
    foreach ($allowed_options as $key => $label) {
      $list[$key] = $label;
    }
    return $list;
  }

}
