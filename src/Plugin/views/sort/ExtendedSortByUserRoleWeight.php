<?php

namespace Drupal\views_sort_options_weight\Plugin\views\sort;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\RoleInterface;

/**
 * Allows setting weight for each User Role.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("extended_sort_by_user_role_weight")
 */
class ExtendedSortByUserRoleWeight extends ExtendedSortByWeightBase {

  /**
   * The User Role entity storage.
   *
   * @var \Drupal\user\RoleStorage
   */
  protected $userRoleStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityFieldManagerInterface $field_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_type_bundle_info, $field_manager);

    $this->userRoleStorage = $this->entityTypeManager->getStorage('user_role');
  }

  /**
   * Get the list of User Roles.
   *
   * @return array
   *   The list of bundles.
   */
  protected function getSortOptionsList() {
    $list = [];

    /** @var \Drupal\user\RoleInterface $role */
    foreach ($this->userRoleStorage->loadMultiple() as $role) {
      switch ($role->id()) {
        case RoleInterface::ANONYMOUS_ID:
          break;

        // A record about user is assigned to the "Authenticated user" is not
        // set to the "user_roles" table so the value will be NULL.
        case RoleInterface::AUTHENTICATED_ID:
          $list[static::EMPTY_VALUE_IDENTIFIER] = $role->label();
          break;

        default:
          $list[$role->id()] = $role->label();
          break;
      }
    }

    return $list;
  }

}
