<?php

namespace Drupal\views_sort_options_weight\Plugin\views\sort;

/**
 * Default implementation of the base sort plugin.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("extended_sort_by_bundles_weight")
 */
class ExtendedSortByBundlesWeight extends ExtendedSortByWeightBase {

  /**
   * Get the list of bundles: {bundle_machine_name} => {bundle label}.
   *
   * @return array
   *   The list of bundles.
   */
  protected function getSortOptionsList() {
    $list = [];
    foreach ($this->entityTypeBundleInfo->getBundleInfo($this->definition['entity_type']) as $bundle_key => $bundle_info) {
      $list[$bundle_key] = $bundle_info['label'];
    }
    return $list;
  }

}
